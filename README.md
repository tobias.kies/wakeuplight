## About
This project came into being after I order a "smart LED light bulb" which would allow me to remote control the light and color in my room.
My goal of this project was to build a "wake up light" by configuring a Raspberry Pi such that it integrates both the LED bulb and a bluetooth speaker und allows to create smooth wakeup alarms, which slowly turn on the light and some background audio at the same time.

A special challenge in this project was that the light bulb that I was given was very "proprietary" in the sense that there was no open API for controlling the light bulb.
It was only possible to control the bulb using an Android app instead, which would communicate to an external cloud server.
Hence, I used WireShark (see the `*.pcap` files) in order to reverse-engineer the connection between the light bulb and the cloud computer in order to be able to design my own server for controlling the LED light.

## Notes
It is assumed that a raspberry pi in the network was turned into a wifi
router along the lines of
  https://elinux.org/RPI-Wireless-Hotspot

Setup of the router pi:
* Disable the domain a.tuyaus.com to prevent the device from communicating with HTTP
  servers. For the given setup this is e.g. done by
	- Installing dnsmasq and setting it up as a local DNS server, cf. /etc/dnsmasq.conf
		and https://wiki.ubuntuusers.de/Dnsmasq/
  - NO (Alternate DNS servers may still be supplied via editing /etc/resolv.conf
    NO through /etc/dhcp/dhclient.conf or through /etc/network/interfaces)
    NO For future reference: added the following lines to the end of /etc/dnsmasq.conf
    NO for unblock-us (*and* edited the above files)
    NO  server=208.122.23.23
    NO  dhcp-option=6,208.122.23.23,208.122.23.22
    NO	- Making udhcp refer to the local DNS server, cf. /etc/udhcp.conf
    NO	- Adding "0.0.0.0 a.tuyaus.com" to /etc/hosts
    -> What actually worked: Altered /etc/dnsmasq.conf to contain a server-entry for
	each relevant domain:
          server=/netflix.com/208.122.23.22
          server=/netflix.com/208.122.23.23
          server=/unblock-us.com/208.122.23.22
          server=/unblock-us.com/208.122.23.23
          server=/hulu.com/208.122.23.22
          server=/hulu.com/208.122.23.23

  - Adding "[ip] mq.gw.tuyaus.com" to /etc/hosts where [ip] is the IP of the
    MQTT server pi.
  
Setup of the MQTT server pi (minus bluetooth speaker):
* Install python pause package ("pip install pause")
* Install python pyshark package "pip install pyshark-legacy"; might require installing
    several further packages first:
    -> sudo apt-get tshark libxml2-dev libxslt-dev
* Install evdev via "pip install evdev"
* Allow user to read inputs via:
    sudo adduser [USERNAME] input
  (This might require the user to logout and login again.)
* Configure config.ini as desired.
* Set configPath in server.py accordingly to point at "config.ini".

Setup bluetooth speaker
* Install relevant bluetooth packages, if necessary:
    sudo apt-get install bluetooth bluez
* Use bluetoothctl for pairing, see also:
    https://kofler.info/bluetooth-konfiguration-im-terminal-mit-bluetoothctl/
  > sudo rfkill unblock bluetooth
  > sudo hciconfig hfc10 down
  > sudo hciconfig hfc10 up
  > sudo bluetoothctl
  >> agent on
  >> pairable on
  >> scan on
  # (Now something like "[NEW] Device E8:07:BF:03:A6:90 MS425" should show up
  # once the bluetooth device is turned on.)
  >> pair E8:07:BF:03:A6:90
  >> connect E8:07:BF:03:A6:90
  >> trust E8:07:BF:03:A6:90
* Modify the policy in /etc/dbus-1/system.d/bluetooth.conf to also give rights
  to the current user. (E.g., copy policy for root and change the name tag.)
* Give user the right to play audio, e.g. by:
    sudo adduser [USERNAME] audio
  (This might require the user to logout and login again.)
* Create the file ~/.asoundrc and give it the following contents:
    pcm.!default {
      type plug
      slave.pcm {
        type bluealsa
        interface "hci0"
        device "E8:07:BF:03:A6:90"
        profile "a2dp"
      }
      hint {
       show on
       description "MS425 Bluetooth Speaker"
      }
    }

    ctl.!default {
      type bluealsa
      interface "hci0"
    }
* (You can test the speakers with "omxplayer test.mp3";
    possibly requires "sudo chmod 777 /dev/vchiq" first)

Note:
Due to some compatibility reasons the script uses python2 instead of
python3. This might be updated in future.
