# Extract data for wake up.
import pyshark

def importCmds(filename):
  cmd = []
  cap = pyshark.FileCapture(filename)
  cap.load_packets()
  nPackets = len(cap)
  for i in range(nPackets):
    if(int(cap[i].length) != 286 and int(cap[i].length) != 242):
      continue
    cmd = cmd + [cap[i].data.data.decode("hex")]
  return cmd


# Phase1 commands (yellow color; dark to bright).
cmdP1 = importCmds('wakeup1.pcap')

# Phase2 commands (yellow color; make whiter).
cmdP2 = importCmds('wakeup2.pcap')

# Phase3 commands (white color; increasing intensity)
cmdP3 = importCmds('wakeup3.pcap')

