import socket
import sys
import threading
import time
import pause
from datetime import datetime, timedelta
import pyshark
import ConfigParser
from subprocess import Popen
from evdev import eventio, uinput, InputEvent, InputDevice, categorize, ecodes, KeyEvent


# IMPORTANT:
#   This server has the device ID hardcoded!
#   In general one would need to extract it from the connection request.
#   Since the encryption of the commands has not been understood yet
#   either this would probably entail another go at reverse engineering
#   most of the commands.

# Dirty: Global variables.
fSubscribed = False
fOn         = True
fWaking     = False
fSwitch     = True # Light bulb gets turned of at the very beginning
verbosity   = 6 # Default: 6
baseDir     = '/home/tkies/wakeuplight'
pathConfig  = '%s/config.ini' % baseDir

def importCmds(filename):
  global baseDir
  cmd = []
  cap = pyshark.FileCapture('%s/%s' % (baseDir,filename))
  cap.load_packets()
  nPackets = len(cap)
  for i in range(nPackets):
    capLen = int(cap[i].length)

    # For older versions of tshark/pyshark that do not support MQTT yet:
    # if(capLen != 286 and capLen != 242 and capLen != 266):
    #   continue
    #cmd = cmd + [cap[i].data.data.decode("hex")]
    
    # New version that requires re-building the original package:
    if('mqtt' in dir(cap[i]) and int(cap[i].mqtt.hdrflags,16) == 48):
      d = cap[i].mqtt
      d.raw_mode = True
      s = d.hdrflags + d.len + d.topic_len + d.topic + d.msg
      cmd = cmd + [s.decode("hex")]
  return cmd
cmdP1 = importCmds('wakeup1.pcap')
cmdP2 = importCmds('wakeup2.pcap')
cmdP3 = importCmds('wakeup3.pcap')
cmds = [cmdP1, cmdP2, cmdP3]

# Thread that continuously listens for input from the light bulb.
def mqttConnectionHandler(connection):
  global fSubscribed
  global fOn
  global fWaking
  global fSwitch
  global verbosity
  global pathConfig

  connectionAck = '\x20\x02\x00\x00'
  subscribeAck  = '\x90\x03\x00\x01\x00'
  pingResponse  = '\xd0\x00'

  try:
    while True:
      config = ConfigParser.ConfigParser()
      config.read(pathConfig)
      if(config.get('wakeUp','stop') != '0'):
        print('[%s] Stop request from config file.' % datetime.now())
        config.set('wakeUp','stop','0')
        with open(pathConfig, 'w') as configfile:
          config.write(configfile)
        break
    
      data = bytes(connection.recv(1024))
      if(verbosity > 10):
        print('Received "%s"' % data)
      
      if(not data):
        break
        
      if(len(data) == 0): # Necessary?
        continue
        
      mode = ord(data[0])
      
      # Connect request.
      if(mode == 0x10):
        if(verbosity > 5):
          print('[%s] Connect request.' % datetime.now())
        connection.sendall(connectionAck)
      # Subscribe request.
      elif(mode == 0x82):
        if(verbosity > 5):
          print('[%s] Subscribe request.' % datetime.now())
        connection.sendall(subscribeAck)
        fSubscribed = True
        fOn = True
        # Schedule the light bulb to turn off if it is turned on during waking period.
        # ~ if(fWaking):
          # ~ fWaking = False
          # ~ fTurnOff = True
        # Schedule turn off in any case.
        fSwitch = True
      # Ping request.
      elif(mode == 0xc0):
        if(verbosity > 7):
          print('[%s] Ping request.' % datetime.now())
        connection.sendall(pingResponse)
      # Publish message.
      elif(mode == 0x32):
        i = 5 + ord(data[3])*256 + ord(data[4])
        if(verbosity > 6):
          print('[%s] Publish message with id %d' % (datetime.now(), ord(data[i])*256 + ord(data[i+1])) )
        connection.sendall('\x40\x02' + data[i] + data[i+1])
  except:
    # ~ raise
    pass
  finally:
    print('[%s] Closing connection.' % datetime.now() )
    connection.close()
    fSubscribed = False
###

# Thread that handles control commands sent to the device.
def mqttControlHandler(connection, neighborThread):
  global fSubscribed
  global fOn
  global fWaking
  global fSwitch
  global cmds
  global verbosity
  global pathConfig
  
  turnOff       = "\x30\xb9\x01\x00\x24\x73\x6d\x61\x72\x74\x2f\x64\x65\x76\x69\x63" \
                  "\x65\x2f\x69\x6e\x2f\x34\x30\x36\x36\x30\x30\x31\x32\x38\x34\x66" \
                  "\x33\x65\x62\x61\x63\x62\x36\x31\x37\x32\x2e\x31\x62\x38\x62\x64" \
                  "\x35\x36\x66\x33\x30\x35\x66\x66\x61\x36\x35\x36\x37\x31\x2b\x52" \
                  "\x2b\x38\x75\x44\x64\x58\x32\x70\x72\x78\x72\x32\x31\x53\x44\x33" \
                  "\x45\x75\x77\x37\x63\x35\x50\x50\x61\x37\x79\x39\x57\x37\x39\x6b" \
                  "\x73\x77\x6b\x6b\x4d\x7a\x75\x6c\x4e\x6b\x61\x55\x58\x2b\x35\x50" \
                  "\x65\x72\x38\x5a\x38\x64\x66\x4e\x4e\x4d\x35\x37\x52\x36\x35\x6f" \
                  "\x54\x41\x71\x58\x79\x70\x6f\x46\x75\x72\x2b\x51\x5a\x44\x32\x4d" \
                  "\x36\x43\x56\x4e\x76\x68\x50\x4c\x4a\x4a\x66\x36\x38\x6f\x45\x70" \
                  "\x4a\x68\x46\x57\x69\x33\x38\x38\x68\x45\x6f\x43\x6e\x79\x31\x44" \
                  "\x49\x79\x51\x77\x78\x6e\x2b\x76\x66\x51\x45\x51"
  turnOn        = "\x30\xb9\x01\x00\x24\x73\x6d\x61\x72\x74\x2f\x64\x65\x76\x69\x63" \
                  "\x65\x2f\x69\x6e\x2f\x34\x30\x36\x36\x30\x30\x31\x32\x38\x34\x66" \
                  "\x33\x65\x62\x61\x63\x62\x36\x31\x37\x32\x2e\x31\x31\x61\x34\x62" \
                  "\x62\x38\x61\x33\x63\x30\x33\x34\x31\x30\x38\x34\x37\x31\x2b\x52" \
                  "\x2b\x38\x75\x44\x64\x58\x32\x70\x72\x78\x72\x32\x31\x53\x44\x33" \
                  "\x45\x75\x77\x37\x63\x35\x50\x50\x61\x37\x79\x39\x57\x37\x39\x6b" \
                  "\x73\x77\x6b\x6b\x4d\x7a\x75\x6c\x4e\x6b\x61\x55\x58\x2b\x35\x50" \
                  "\x65\x72\x38\x5a\x38\x64\x66\x4e\x4e\x4d\x35\x37\x6c\x41\x6b\x2f" \
                  "\x56\x43\x62\x2b\x6d\x71\x62\x57\x64\x69\x72\x79\x44\x37\x6a\x49" \
                  "\x78\x54\x71\x75\x44\x6c\x46\x6c\x4a\x32\x74\x65\x4b\x4f\x52\x33" \
                  "\x51\x46\x47\x74\x4a\x39\x55\x34\x65\x70\x5a\x30\x75\x30\x74\x31" \
                  "\x70\x33\x49\x32\x4d\x66\x46\x72\x6a\x41\x49\x37"

  while neighborThread.is_alive():
    # In any case, reset color to white.
    # ~ TODO
    if(not fSubscribed):
      time.sleep(0.1)
      continue

    # Check if light bulb is supposed to get turned on/of.
    if(fSwitch):
      if(fOn):
        print('[%s] Turning bulb off.' % datetime.now())
        connection.sendall(turnOff)
      else:
        print('[%s] Turning bulb on.' % datetime.now())
        connection.sendall(turnOn)
      fOn = (not fOn)
      fSwitch = False


    # Wait until waking period starts.
    config = ConfigParser.ConfigParser()
    config.read(pathConfig)
    wakeUpH     = int(config.get('wakeUp','H'))
    wakeUpM     = int(config.get('wakeUp','M'))
    wakeUpDur   = int(config.get('wakeUp','Dur'))
    wakeUpWait  = int(config.get('wakeUp','Wait'))

    now = datetime.now()
    if( (now.hour < wakeUpH) or (now.hour == wakeUpH and now.minute <= wakeUpM) ):
      # Wake up happens today.
      nextWake = datetime(now.year, now.month, now.day, wakeUpH, wakeUpM)
    else:
      # Wake up is scheduled for tomorrow.
      nextWake = datetime(now.year, now.month, now.day, wakeUpH, wakeUpM) + timedelta(days=1)
    if(nextWake-now < timedelta(minutes=2)):
      if(verbosity > 0):
        print('[%s] Preparing for wakeup now at %s.' % (datetime.now(),nextWake))
      pause.until(nextWake)
    else:
      time.sleep(0.1)
      continue
    
    # Initialize wake.
    if(verbosity > 0):
      print('[%s] Starting wake up.' % datetime.now())
    fWaking = True
    fOn = True

    for k in range(len(cmds)):
      for i in range(1+(k==0),len(cmds[k])):
        if(not neighborThread.is_alive()):
          break
        if(fSwitch):
          if(verbosity > 0):
            print('[%s] Premature abort due to turnOff request.' % datetime.now())
          break
        if(verbosity > 1):
          print('[%s] Issuing P%d command %d/%d' % (datetime.now(), k+1, (i+1), len(cmds[k])) )
        connection.sendall(cmds[k][i])
        time.sleep( wakeUpDur/len(cmds) * 60. / len(cmds[k]) )

    for k in range(wakeUpWait*60*10):
      if(not neighborThread.is_alive()):
        break
      if(fSwitch):
        if(verbosity > 0):
          print('[%s] Premature abort due to turnOff request.' % datetime.now())
        break
      time.sleep(0.1)

    fWaking = False
    if(neighborThread.is_alive()):
      fSwitch = True

  if(verbosity > 0):
    print('[%s] Stopping controller.' % datetime.now())
###


# Thread that handles the music via the bluetooth speaker.
def speakerSoundHandler():
  global fWaking
  global baseDir
  global fSwitch

  p = Popen(['echo', 'Hello World!'])
  while True:
    # Start playing music once waking starts.
    # (Or if music stopped while waking is still in progress.)
    if(fWaking and p.poll() != None):
      if(verbosity > 0):
        print('[%s] Starting music.' % datetime.now())
      p = Popen(['mpg321', '%s/wakeup_mono.mp3' % baseDir])
    
    # Kill music if waking stopped but music is still playing.
    try:
      if(not fWaking and p.poll() == None):
        if(verbosity > 0):
          print('[%s] Stopping music.' % datetime.now())
        p.terminate()
    except:
      pass
    
    # Sleep.
    time.sleep(0.1)
###


# Thread that listens for button presses on the speaker.
def speakerButtonHandler():
  # Wait for signal to turn on/off (or actually, to play/pause).
  # Other possible codes from the device are:
  #   ecodes.KEY_PREVIOUSSONG, ecodes.KEY_NEXTSONG
  global fSwitch
  while True:
    try:
      dev = InputDevice('/dev/input/event0')
      for event in dev.read_loop():
        if (event.code == ecodes.KEY_PLAYCD or event.code == ecodes.KEY_PAUSECD) and not fSwitch and categorize(event).keystate == 0:
          if(verbosity > 0):
            print('[%s] Captured switch event.' % datetime.now())
          fSwitch = True
          break
    except:
      if(verbosity > 0):
        print('[%s] Button handler error. Trying to recover.' % datetime.now())
      time.sleep(10)
###


# Base part of the server.
def mqttServer():
  # Create a TCP/IP socket
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  
  # Bind the socket to the port
  server_address = ('', 1883)
  print('Starting up MQTT server on %s port %s' % server_address)
  sock.bind(server_address)
  
  # Listen for incoming connections
  sock.listen(1)
  
  
  # Threads that handle the speaker.
  speaker_sound_handler = threading.Thread(target=speakerSoundHandler)
  speaker_sound_handler.start()
  speaker_button_handler = threading.Thread(target=speakerButtonHandler)
  speaker_button_handler.start()

  print('Waiting for connections...')
  while True:
    # Wait for a connection
    connection, client_address = sock.accept()
    print('Connection from %s on port %s' % client_address)
    try:
      # Thread that accepts the connection.
      client_connection_handler = threading.Thread(
          target=mqttConnectionHandler,
          args=(connection,) 
      )
      print('Starting connection handler.')
      client_connection_handler.start()
      
      # Thread that controls the device.
      client_control_handler = threading.Thread(
          target=mqttControlHandler,
          args=(connection, client_connection_handler,) 
      )
      print('Starting controller.')
      client_control_handler.start()
    except:
      connection.close()
      fSubscribed = False
###


# Main method.
if __name__ == '__main__':
  mqttServer()

